#!/usr/bin/python
# sttr1.py - python port of original ascii star trek game
# original BASIC source included in comments

#REM  Extracted from HP tape image 16-Nov-2003 by Pete Turnbull
#
#1  REM ****  HP BASIC PROGRAM LIBRARY  ******************************
#2  REM
#3  REM       STTR1: STAR TREK
#4  REM
#5  REM       36243  REV B  --  10/73
#6  REM
#7  REM ****  CONTRIBUTED PROGRAM  ***********************************
#100  REM *****************************************************************
#110  REM ***                                                           ***
#120  REM ***     STAR TREK: BY MIKE MAYFIELD, CENTERLINE ENGINEERING   ***
#130  REM ***                                                           ***
#140  REM ***        TOTAL INTERACTION GAME - ORIG. 20 OCT 1972
#150  REM ***                                                           ***
#160  REM *****************************************************************

import sys
import numpy
import random
import datetime
import binascii
import math

# Original Variables
# T = Stardate
# C$ = Condition
# Q1,Q2 = Current Quadrant
# S1,S2 = Current Sector
# E = Energy
# P = Photon Torpedos
# S = Shields
# D[] = Damage status
# K9 = Total Klingons
# B9 = Total Bases
# T9 = TTL

# SET COURSE
#1410  PRINT "COURSE (1-9):";
#1420  INPUT C1
#1430  IF C1=0 THEN 1270
#1440  IF C1<1 OR C1 >= 9 THEN 1410
#1450  PRINT "WARP FACTOR (0-8):";
#1460  INPUT W1
#1470  IF W1<0 OR W1>8 THEN 1410
#1480  IF D[1] >= 0 OR W1 <= .2 THEN 1510
#1490  PRINT "WARP ENGINES ARE DAMAGED, MAXIMUM SPEED = WARP .2"
#1500  GOTO 1410
#1510  IF K3 <= 0 THEN 1560
#1520  GOSUB 3790
#1530  IF K3 <= 0 THEN 1560
#1540  IF S<0 THEN 4000
#1550  GOTO 1610
#1560  IF E>0 THEN 1610
#1570  IF S<1 THEN 3920
#1580  PRINT "YOU HAVE"E" UNITS OF ENERGY"
#1590  PRINT "SUGGEST YOU GET SOME FROM YOUR SHIELDS WHICH HAVE"S" UNITS LEFT"
#1600  GOTO 1270
#1610  FOR I=1 TO 8
#1620  IF D[I] >= 0 THEN 1640
#1630  D[I]=D[I]+1
#1640  NEXT I
#1650  IF RND(1)>.2 THEN 1810
#1660  R1=INT(RND(1)*8+1)
#1670  IF RND(1) >= .5 THEN 1750
#1680  D[R1]=D[R1]-(RND(1)*5+1)
#1690  PRINT
#1700  PRINT "DAMAGE CONTROL REPORT:";
#1710  GOSUB 5610
#1720  PRINT " DAMAGED"
#1730  PRINT
#1740  GOTO 1810
#1750  D[R1]=D[R1]+(RND(1)*5+1)
#1760  PRINT
#1770  PRINT "DAMAGE CONTROL REPORT:";
#1780  GOSUB 5610
#1790  PRINT " STATE OF REPAIR IMPROVED"
#1800  PRINT
#1810  N=INT(W1*8)
#1820  A$="   "
#1830  Z1=S1
#1840  Z2=S2
#1850  GOSUB 5510
#1870  X=S1
#1880  Y=S2
#1885  C2=INT(C1)
#1890  X1=C[C2,1]+(C[C2+1,1]-C[C2,1])*(C1-C2)
#1900  X2=C[C2,2]+(C[C2+1,2]-C[C2,2])*(C1-C2)
#1910  FOR I=1 TO N
#1920  S1=S1+X1
#1930  S2=S2+X2
#1940  IF S1<.5 OR S1 >= 8.5 OR S2<.5 OR S2 >= 8.5 THEN 2170
#1950  A$="   "
#1960  Z1=S1
#1970  Z2=S2
#1980  GOSUB 5680
#1990  IF Z3 <> 0 THEN 2070
#2030  PRINT  USING 5370;S1,S2
#2040  S1=S1-X1
#2050  S2=S2-X2
#2060  GOTO 2080
#2070  NEXT I
#2080  A$="<*>"
#2083  S1=INT(S1+.5)
#2086  S2=INT(S2+.5)
#2090  Z1=S1
#2100  Z2=S2
#2110  GOSUB 5510
#2120  E=E-N+5
#2130  IF W1<1 THEN 2150
#2140  T=T+1
#2150  IF T>T0+T9 THEN 3970
#2160  GOTO 1260
#2170  X=Q1*8+X+X1*N
#2180  Y=Q2*8+Y+X2*N
#2190  Q1=INT(X/8)
#2200  Q2=INT(Y/8)
#2210  S1=INT(X-Q1*8+.5)
#2220  S2=INT(Y-Q2*8+.5)
#2230  IF S1 <> 0 THEN 2260
#2240  Q1=Q1-1
#2250  S1=8
#2260  IF S2 <> 0 THEN 2290
#2270  Q2=Q2-1
#2280  S2=8
#2290  T=T+1
#2300  E=E-N+5
#2310  IF T>T0+T9 THEN 3970
#2320  GOTO 810
def set_course():
  global E
  global Q1
  global Q2
  global S1
  global S2
  global T
  global Qstr
  global Rstr
  global Sstr
  global D
  print "COURSE (1-9):"
  while True:
    try:
      C1 = float(raw_input(""))
      break
    except:
      pass
  if C1 == 0:
    return
  if C1 < 1 or C1 >= 9:
    set_course()
    return
  print "WARP FACTOR (0-8):"
  while True:
    try:
      W1 = float(raw_input(""))
      break
    except:
      pass
  if W1 < 0 or W1 > 8:
    set_course()		# convert to while?
    return
  if D[0] < 0 and W1 > .2:
    print "WARP ENGINES ARE DAMAGED, MAXIMUM SPEED = WARP .2"
    set_course()
    return
  if K3 > 0:
    klingon_attack()
    if S < 0:
      ship_destroyed()
  if E <= 0:
    if S >= 1:
      print "YOU HAVE " + str(E) + " UNITS OF ENERGY"
      print "SUGGEST YOU GET SOME FROM YOUR SHIELDS WHICH HAVE " + str(S) + " UNITS LEFT"
      return
    else:
      ship_adrift()
  for i in range(8):
    if D[i] < 0:
      D[i] = D[i] + 1
  if random.random() <= .2:
    R1 = int(random.random()*8)
    if R1 < .5:
      D[R1] = D[R1] - (random.random()*5)
      print
      print "DAMAGE CONTROL REPORT:"
      damage_ctrl()
      print " DAMAGED"
      print
    else:
      D[R1] = D[R1] + (random.random()*5)
      print
      print "DAMAGE CONTROL REPORT:"
      damage_ctrl()
      print " STATE OF REPAIR IMPROVED"
      print
  N = int(W1*8)
  Astr = "   "
  Z1 = int(S1)
  Z2 = int(S2)
  Qstr,Rstr,Sstr = str_array_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
  X = S1
  Y = S2
  C2 = int(C1)
  X1 = C[C2-1,0]+(C[C2-1+1,0]-C[C2-1,0])*(C1-C2)
  X2 = C[C2-1,1]+(C[C2-1+1,1]-C[C2-1,1])*(C1-C2)
  for i in range(N):
    S1 = S1 + X1
    S2 = S2 + X2
    #if S1 >= -.5 and S1 < 7.5 and S2 >= -.5 and S2 < 7.5:
    if S1 >= 0 and S1 < 7 and S2 >= 0 and S2 < 7:
      Astr = "   "
      Z1 = int(S1)
      Z2 = int(S2)
      Z3 = str_cmp_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
      if Z3 is 1:
        print " WARP ENGINES SHUTDOWN AT SECTOR %d,%d DUE TO BAD NAVIGATION" % (S1,S2)
        S1 = S1 - X1
        S2 = S2 - X2
        break
    else:
      X = (Q1+1) * 8 + X + X1 * N
      Y = (Q2+1) * 8 + Y + X2 * N
      Q1 = int((X/8)-1)
      Q2 = int((Y/8)-1)
      S1 = int(X-(Q1+1)*8)
      S2 = int(Y-(Q2+1)*8)
      if S1 is -1:
        Q1 = Q1 - 1
        S1 = 7
      if S2 is -1:
        Q2 = Q2 - 1
        S2 = 7
      T = T + 1
      E = E - N + 5
      if T > T0 + T9:
        time_out()
      draw_current_sector()
      return
  Astr = "<*>"
  S1 = int(S1 + .5)
  S2 = int(S2 + .5)
  Z1 = S1
  Z2 = S2
  Qstr,Rstr,Sstr = str_array_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
  E = E - N + 5
  if W1 >= 1:
    T = T + 1
    if T > T0 + T9:
      time_out()
    return

# SHORT RANGE SENSOR SCAN
#4120  FOR I=S1-1 TO S1+1
#4130  FOR J=S2-1 TO S2+1
#4140  IF I<1 OR I>8 OR J<1 OR J>8 THEN 4200
#4150  A$=">!<"
#4160  Z1=I
#4170  Z2=J
#4180  GOSUB 5680
#4190  IF Z3=1 THEN 4240
#4200  NEXT J
#4210  NEXT I
#4220  D0=0
#4230  GOTO 4310
#4240  D0=1
#4250  C$="DOCKED"
#4260  E=3000
#4270  P=10
#4280  PRINT "SHIELDS DROPPED FOR DOCKING PURPOSES"
#4290  S=0
#4300  GOTO 4380
#4310  IF K3>0 THEN 4350
#4320  IF E<E0*.1 THEN 4370
#4330  C$="GREEN"
#4340  GOTO 4380
#4350  C$="RED"
#4360  GOTO 4380
#4370  C$="YELLOW"
#4380  IF D[2] >= 0 THEN 4430
#4390  PRINT
#4400  PRINT "*** SHORT RANGE SENSORS ARE OUT ***"
#4410  PRINT
#4420  GOTO 4530
#4430  PRINT  USING 4540
#4440  PRINT  USING 4550;Q$[1,3],Q$[4,6],Q$[7,9],Q$[10,12],Q$[13,15],Q$[16,18],Q$[19,21],Q$[22,24]
#4450  PRINT  USING 4560;Q$[25,27],Q$[28,30],Q$[31,33],Q$[34,36],Q$[37,39],Q$[40,42],Q$[43,45],Q$[46,48],T
#4460  PRINT  USING 4570;Q$[49,51],Q$[52,54],Q$[55,57],Q$[58,60],Q$[61,63],Q$[64,66],Q$[67,69],Q$[70,72],C$
#4470  PRINT  USING 4580;R$[1,3],R$[4,6],R$[7,9],R$[10,12],R$[13,15],R$[16,18],R$[19,21],R$[22,24],Q1,Q2
#4480  PRINT  USING 4590;R$[25,27],R$[28,30],R$[31,33],R$[34,36],R$[37,39],R$[40,42],R$[43,45],R$[46,48],S1,S2
#4490  PRINT  USING 4600;R$[49,51],R$[52,54],R$[55,57],R$[58,60],R$[61,63],R$[64,66],R$[67,69],R$[70,72],E
#4500  PRINT  USING 4610;S$[1,3],S$[4,6],S$[7,9],S$[10,12],S$[13,15],S$[16,18],S$[19,21],S$[22,24],P
#4510  PRINT  USING 4620;S$[25,27],S$[28,30],S$[31,33],S$[34,36],S$[37,39],S$[40,42],S$[43,45],S$[46,48],S
#4520  PRINT  USING 4540
#4530  RETURN 
#4540  IMAGE  "---------------------------------"
#4550  IMAGE  8(X,3A)
#4560  IMAGE  8(X,3A),8X,"STARDATE",8X,5D
#4570  IMAGE  8(X,3A),8X,"CONDITION",8X,6A
#4580  IMAGE  8(X,3A),8X,"QUADRANT",9X,D,",",D
#4590  IMAGE  8(X,3A),8X,"SECTOR",11X,D,",",D
#4600  IMAGE  8(X,3A),8X,"ENERGY",9X,6D
#4610  IMAGE  8(X,3A),8X,"PHOTON TORPEDOES",3D
#4620  IMAGE  8(X,3A),8X,"SHIELDS",8X,6D
def srs_scan():
  global E
  global P
  global S
  global Cstr
  D0 = 0
  for i in (S1-1,S1,S1+1):
    for j in (S2-1,S2,S2+1):
      if i >= 0 and i <= 7 and j >= 0 and j <= 7:
        Astr = ">!<"
        Z1 = int(i)
        Z2 = int(j)
        Z3 = str_cmp_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
        if Z3 is 0:
          D0 = 1
          Cstr = "DOCKED"
          E = 3000
          P = 10
          print "SHIELDS DROPPED FOR DOCKING PURPOSES"
          S = 0
  if K3 > 0:
    Cstr = "RED"
  elif E < E0 * .1:
    Cstr = "YELLOW"
  else:
    Cstr = "GREEN"
  if D[1] < 0:
    print
    print "*** SHORT RANGE SENSORS ARE OUT ***"
    print
    return
  print "---------------------------------"
  print " %3s %3s %3s %3s %3s %3s %3s %3s" % (Qstr[0:3], Qstr[3:6], Qstr[6:9], Qstr[9:12], Qstr[12:15], Qstr[15:18], Qstr[18:21], Qstr[21:24])
  print " %3s %3s %3s %3s %3s %3s %3s %3s        STARDATE        %5d" % (Qstr[24:27], Qstr[27:30], Qstr[30:33], Qstr[33:36], Qstr[36:39], Qstr[39:42], Qstr[42:45], Qstr[45:48], T)
  print " %3s %3s %3s %3s %3s %3s %3s %3s        CONDITION        %-6s" % (Qstr[48:51], Qstr[51:54], Qstr[54:57], Qstr[57:60], Qstr[60:63], Qstr[63:66], Qstr[66:69], Qstr[69:72], Cstr)
  print " %3s %3s %3s %3s %3s %3s %3s %3s        QUADRANT         %d,%d" % (Rstr[0:3], Rstr[3:6], Rstr[6:9], Rstr[9:12], Rstr[12:15], Rstr[15:18], Rstr[18:21], Rstr[21:24], Q1, Q2)
  print " %3s %3s %3s %3s %3s %3s %3s %3s        SECTOR           %d,%d" % (Rstr[24:27], Rstr[27:30], Rstr[30:33], Rstr[33:36], Rstr[36:39], Rstr[39:42], Rstr[42:45], Rstr[45:48], S1, S2)
  print " %3s %3s %3s %3s %3s %3s %3s %3s        ENERGY         %6d" % (Rstr[48:51], Rstr[51:54], Rstr[54:57], Rstr[57:60], Rstr[60:63], Rstr[63:66], Rstr[66:69], Rstr[69:72], E)
  print " %3s %3s %3s %3s %3s %3s %3s %3s        PHOTON TORPEDOS %3d" % (Sstr[0:3], Sstr[3:6], Sstr[6:9], Sstr[9:12], Sstr[12:15], Sstr[15:18], Sstr[18:21], Sstr[21:24], P)
  print " %3s %3s %3s %3s %3s %3s %3s %3s        SHIELDS        %6d" % (Sstr[24:27], Sstr[27:30], Sstr[30:33], Sstr[33:36], Sstr[36:39], Sstr[39:42], Sstr[42:45], Sstr[45:48], S)
  print "---------------------------------"

# LONG RANGE SENSOR SCAN
#2330  IF D[3] >= 0 THEN 2370
#2340  PRINT "LONG RANGE SENSORS ARE INOPERABLE"
#2350  IMAGE  "LONG RANGE SENSOR SCAN FOR QUADRANT",D,",",D
#2360  GOTO 1270
#2370  PRINT  USING 2350;Q1,Q2
#2380  PRINT  USING 2520
#2390  FOR I=Q1-1 TO Q1+1
#2400  MAT N=ZER
#2410  FOR J=Q2-1 TO Q2+1
#2420  IF I<1 OR I>8 OR J<1 OR J>8 THEN 2460
#2430  N[J-Q2+2]=G[I,J]
#2440  IF D[7]<0 THEN 2460
#2450  Z[I,J]=G[I,J]
#2460  NEXT J
#2470  PRINT  USING 2510;N[1],N[2],N[3]
#2480  PRINT  USING 2520
#2490  NEXT I
#2500  GOTO 1270
#2510  IMAGE  ": ",3(3D," :")
#2520  IMAGE  "-----------------"
def lrs_scan():
  global G
  global Z
  if D[2] < 0:
    print "LONG RANGE SENSORS ARE INOPERABLE"
    return
  print "LONG RANGE SENSOR SCAN FOR QUADRANT " + str(Q1) + "," + str(Q2)
  print "-----------------"
  for i in (Q1-1,Q1,Q1+1):
    N = numpy.zeros(3)
    for j in (Q2-1,Q2,Q2+1):
      if i >= 0 and i <= 7 and j >= 0 and j <= 7:
        N[j-Q2+1] = G[i,j]
        if D[7] >= 0:
          Z[i,j] = G[i,j]
    print ": %03d :%03d :%03d :" % (N[0],N[1],N[2])
  print "-----------------"

# FIRE PHASERS
#2530  IF K3 <= 0 THEN 3670
#3670  PRINT "SHORT RANGE SENSORS REPORT NO KLINGONS IN THIS QUADRANT"
#3680  GOTO 1270
#2540  IF D[4] >= 0 THEN 2570
#2550  PRINT "PHASER CONTROL IS DISABLED"
#2560  GOTO 1270
#2570  IF D[7] >= 0 THEN 2590
#2580  PRINT " COMPUTER FAILURE HAMPERS ACCURACY"
#2590  PRINT "PHASERS LOCKED ON TARGET.  ENERGY AVAILABLE="E
#2600  PRINT "NUMBER OF UNITS TO FIRE:";
#2610  INPUT X
#2620  IF X <= 0 THEN 1270
#2630  IF E-X<0 THEN 2570
#2640  E=E-X
#2650  GOSUB 3790
#2660  IF D[7] >= 0 THEN 2680
#2670  X=X*RND(1)
#2680  FOR I=1 TO 3
#2690  IF K[I,3] <= 0 THEN 2770
#2700  H=(X/K3/FND(0))*(2*RND(1))
#2710  K[I,3]=K[I,3]-H
#2720  PRINT  USING 2730;H,K[I,1],K[I,2],K[I,3]
#2730  IMAGE  4D," UNIT HIT ON KLINGON AT SECTOR ",D,",",D,"   (",3D," LEFT)"
#2740  IF K[I,3]>0 THEN 2770
#2750  GOSUB 3690
#2760  IF K9 <= 0 THEN 4040
#2770  NEXT I
#2780  IF E<0 THEN 4000
#2790  GOTO 1270
def fire_phasers():
  global E
  global K3
  if K3 <= 0:
    print "SHORT RANGE SENSORS REPORT NO KLINGONS IN THIS QUADRANT"
    return
  if D[3] < 0:
    print "PHASER CONTROL IS DISABLED"
    return
  if D[7] < 0:
    print "COMPUTER FAILURE HAMPERS ACCURACY"
  print "PHASERS LOCKED ON TARGET.  ENERGY AVAILABLE =" + str(E)
  print "NUMBER OF UNITS TO FIRE:"
  while True:
    try:
      X = int(raw_input(""))
      break
    except:
      pass
  if X > 0:
    if E - X >= 0:
      E = E - X
      klingon_attack()
      if D[6] < 0:
        X = X*random.random()
      for i in range(3):
        if K[i,2] > 0:
          H = (X/K3/fnd(i)) * (2*random.random())
          K[i,2] = K[i,2] - H
          print "%4d UNIT HIT ON KLINGON AT SECTOR %d,%d   (%3d LEFT)" %(H,K[i,0],K[i,1],K[i,2])
          if K[i,2] <= 0:
            klingon_destroyed(i)
            if K9 <= 0:
              end_game()
      if E < 0:
        ship_destroyed()
    else:
      fire_phasers()
      return
  else:
    return

# FIRE PHOTON TORPEDOS
#2800  IF D[5] >= 0 THEN 2830
#2810  PRINT "PHOTON TUBES ARE NOT OPERATIONAL"
#2820  GOTO 1270
#2830  IF P>0 THEN 2860
#2840  PRINT "ALL PHOTON TORPEDOES EXPENDED"
#2850  GOTO 1270
#2860  PRINT "TORPEDO COURSE (1-9):";
#2870  INPUT C1
#2880  IF C1=0 THEN 1270
#2890  IF C1<1 OR C1 >= 9 THEN 2860
#2895  C2=INT(C1)
#2900  X1=C[C2,1]+(C[C2+1,1]-C[C2,1])*(C1-C2)
#2910  X2=C[C2,2]+(C[C2+1,2]-C[C2,2])*(C1-C2)
#2920  X=S1
#2930  Y=S2
#2940  P=P-1
#2950  PRINT "TORPEDO TRACK:"
#2960  X=X+X1
#2970  Y=Y+X2
#2980  IF X<.5 OR X >= 8.5 OR Y<.5 OR Y >= 8.5 THEN 3420
#2990  PRINT  USING 3000;X,Y
#3000  IMAGE  15X,D,",",D
#3010  A$="   "
#3020  Z1=X
#3030  Z2=Y
#3040  GOSUB 5680
#3050  IF Z3=0 THEN 3070
#3060  GOTO 2960
#3070  A$="+++"
#3080  Z1=X
#3090  Z2=Y
#3100  GOSUB 5680
#3110  IF Z3=0 THEN 3220
#3120  PRINT "*** KLINGON DESTROYED ***"
#3130  K3=K3-1
#3140  K9=K9-1
#3150  IF K9 <= 0 THEN 4040
#3160  FOR I=1 TO 3
#3170  IF INT(X+.5) <> K[I,1] THEN 3190
#3180  IF INT(Y+.5)=K[I,2] THEN 3200
#3190  NEXT I
#3200  K[I,3]=0
#3210  GOTO 3360
#3220  A$=" * "
#3230  Z1=X
#3240  Z2=Y
#3250  GOSUB 5680
#3260  IF Z3=0 THEN 3290
#3270  PRINT "YOU CAN'T DESTROY STARS SILLY"
#3280  GOTO 3420
#3290  A$=">!<"
#3300  Z1=X
#3310  Z2=Y
#3320  GOSUB 5680
#3330  IF Z3=0 THEN 2960
#3340  PRINT "*** STAR BASE DESTROYED ***  .......CONGRATULATIONS"
#3350  B3=B3-1
#3360  A$="   "
#3370  Z1=INT(X+.5)
#3380  Z2=INT(Y+.5)
#3390  GOSUB 5510
#3400  G[Q1,Q2]=K3*100+B3*10+S3
#3410  GOTO 3430
#3420  PRINT "TORPEDO MISSED"
#3430  GOSUB 3790
#3440  IF E<0 THEN 4000
#3450  GOTO 1270
def fire_photons():
  global P
  global Qstr
  global Rstr
  global Sstr
  global Astr
  global Z1
  global Z2
  global K3
  global K9
  global B3
  if D[4] < 0:
    print "PHOTON TUBES ARE NOT OPERATIONAL"
    return
  if P <= 0:
    print "ALL PHOTON TORPEDOS ARE EXPENDED"
    return
  print "TORPEDO COURSE (1-9):"
  while True:
    try:
      C1 = float(raw_input(""))
      break
    except:
      pass
  if C1 is 0:
    return
  if C1 >= 1 and C1 < 9:
    C2 = int(C1)
    X1 = C[C2-1,0]+(C[C2,0]-C[C2-1,0])*(C1-C2)
    X2 = C[C2-1,1]+(C[C2,1]-C[C2-1,1])*(C1-C2)
    X = S1
    Y = S2
    P = P - 1
    print "TORPEDO TRACK:"
    Z3 = 1
    while Z3 is 1:
      X = X + X1
      Y = Y + X2
      if X >= 0 and X <= 7 and Y >= 0 and Y <= 7:
        print "               %d,%d" % (X,Y)
        Astr = "   "
        Z1 = int(X)
        Z2 = int(Y)
        Z3 = str_cmp_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
        if Z3 is 0:
          next
        Astr = "+++"
        Z1 = int(X)
        Z2 = int(Y)
        Z3 = str_cmp_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
        if Z3 is 0:
          print "*** KLINGON DESTROYED ***"
          K3 = K3 - 1
          K9 = K9 - 1
          if K9 <= 0:
            end_game()
          for i in range(3):
            if int(X) is K[i,0]:
              if int(Y) is K[i,1]:
                K[i,2] = 0
          Astr = "   "
          Qstr,Rstr,Sstr = str_array_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
          G[Q1,Q2] = K3 * 100 + B3 * 10 + S3
          klingon_attack()
          if E < 0:
            ship_destroyed()
          return
        Astr = " * "
        Z1 = int(X)
        Z2 = int(Y)
        Z3 = str_cmp_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
        if Z3 is 0:
          print "YOU CAN'T DESTROY STARS SILLY"
          print "TORPEDO MISSED"
          klingon_attack()
          if E < 0:
            ship_destroyed()
          return
        Astr = ">!<"
        Z1 = int(X)
        Z2 = int(Y)
        Z3 = str_cmp_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
        if Z3 is 0:
          print "*** STAR BASE DESTROYED *** .......CONGRATULATIONS"
          B3 = B3 - 1
          Astr = "   "
          Z1 = int(X)
          Z2 = int(Y)
          Qstr,Rstr,Sstr = str_array_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
          G[Q1,Q2] = K3 * 100 + B3 * 10 + S3
          klingon_attack()
          if E < 0:
            ship_destroyed()
          return
      else:
        Z3 = 0
        print "TORPEDO MISSED"
        klingon_attack()
        if E < 0:
          ship_destroyed()
        return

# SHIELD CONTROL
#3460  IF D[7] >= 0 THEN 3490
#3470  PRINT "SHIELD CONTROL IS NON-OPERATIONAL"
#3480  GOTO 1270
#3490  PRINT "ENERGY AVAILABLE ="E+S"   NUMBER OF UNITS TO SHIELDS:";
#3500  INPUT X
#3510  IF X <= 0 THEN 1270
#3520  IF E+S-X<0 THEN 3490
#3530  E=E+S-X
#3540  S=X
#3550  GOTO 1270
def shield_ctrl():
  global E
  global S
  if D[6] < 0:
    print "SHIELD CONTROL IS NON-OPERATIONAL"
    return
  else:
    print "ENERGY AVAILABLE =" + str(E+S) +  "   NUMBER OF UNITS TO SHIELDS:"
  while True:
    try:
      X = int(raw_input(""))
      break
    except:
      pass
  if X > 0:
    if E + S - X >= 0:
      E = E + S - X
      S = X
  return

# DAMAGE CONTROL REPORT
#3560  IF D[6] >= 0 THEN 3590
#3570  PRINT "DAMAGE CONTROL REPORT IS NOT AVAILABLE"
#3580  GOTO 1270
#3590  PRINT
#3600  PRINT "DEVICE        STATE OF REPAIR"
#3610  FOR R1=1 TO 8
#3620  GOSUB 5610
#3630  PRINT "",D[R1]
#3640  NEXT R1
#3650  PRINT
#3660  GOTO 1270
def damage_ctrl():
  if D[5] < 0:
    print "DAMAGE CONTROL REPORT IS NOT AVAILABLE"
    return
  else:
    print
    print "DEVICE        STATE OF REPAIR"
    for R1 in range(8):
      print_device_name(R1+1)
      print " " + str(int(D[R1]))
    print

#3690  PRINT  USING 3700;K[I,1],K[I,2]
#3700  IMAGE  "KLINGON AT SECTOR ",D,",",D," DESTROYED ****"
#3710  K3=K3-1
#3720  K9=K9-1
#3730  A$="   "
#3740  Z1=K[I,1]
#3750  Z2=K[I,2]
#3760  GOSUB 5510
#3770  G[Q1,Q2]=K3*100+B3*10+S3
#3780  RETURN
def klingon_destroyed(i):
  global K3
  global K9
  global B3
  global S3
  global G
  global Qstr
  global Rstr
  global Sstr
  print "KLINGON AT SECTOR %d,%d DESTROYED ****" % (K[i,0],K[i,1])
  K3 = K3 - 1
  K9 = K9 - 1
  Astr = "   "
  Z1 = int(K[i,0])
  Z2 = int(K[i,1])
  Qstr,Rstr,Sstr = str_array_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
  G[Q1,Q2] = K3 * 100 + B3 * 10 + S3

# Klingon Attack
#3790  IF C$ <> "DOCKED" THEN 3820
#3800  PRINT "STAR BASE SHIELDS PROTECT THE ENTERPRISE"
#3810  RETURN
#3820  IF K3 <= 0 THEN 3910
#3830  FOR I=1 TO 3
#3840  IF K[I,3] <= 0 THEN 3900
#3850  H=(K[I,3]/FND(0))*(2*RND(1))
#3860  S=S-H
#3870  PRINT  USING 3880;H,K[I,1],K[I,2],S
#3880  IMAGE  4D," UNIT HIT ON ENTERPRISE AT SECTOR ",D,",",D,"   (",4D," LEFT)"
#3890  IF S<0 THEN 4000
#3900  NEXT I
#3910  RETURN
def klingon_attack():
  global S
  if Cstr is "DOCKED":
    print "STAR BASE SHIELDS PROTECT THE ENTERPRISE"
    return
  if K3 > 0:
    for i in range(3):
      if K[i,2] > 0:
        H = (K[i,2]/fnd(i)) * ( 2 * random.random())
        S = S - H
        print "%4d UNIT HIT ON ENTERPRISE AT SECTOR %d %d   (%04d LEFT)" % (H,K[i,0],K[i,1],S)
        if S < 0:
          ship_destroyed()
  return

#3970  PRINT
#3980  PRINT "IT IS STARDATE"T
#3990  GOTO 4020
def time_out():
  print
  print "IT IS STARDATE" + T
  print "THERE ARE STILL %d KLINGON BATTLE CRUISERS" % K9
  exit()

#3920  PRINT "THE ENTERPRISE IS DEAD IN SPACE.  IF YOU SURVIVE ALL IMPENDING"
#3930  PRINT "ATTACK YOU WILL BE DEMOTED TO THE RANK OF PRIVATE"
#3940  IF K3 <= 0 THEN 4020
#3950  GOSUB 3790
#3960  GOTO 3940
def ship_adrift():
  print "THE ENTERPRISE IS DEAD IN SPACE.  IF YOU SURVIVE ALL IMPENDING"
  print "ATTACK YOU WILL BE DEMOTED TO THE RANK OF PRIVATE"
  while K3 > 0:
    klingon_attack()
  time_out()

#4000  PRINT
#4010  PRINT "THE ENTERPRISE HAS BEEN DESTROYED.  THE FEDERATION WILL BE CONQUERED"
#4020  PRINT "THERE ARE STILL"K9" KLINGON BATTLE CRUISERS"
#4030  GOTO 230
def ship_destroyed():
  print
  print "THE ENTERPRISE HAS BEEN DESTROYED.  THE FEDERATION WILL BE CONQUERED"
  print "THERE ARE STILL %d KLINGON BATTLE CRUISERS" % K9
  exit()

#4040  PRINT
#4050  PRINT "THE LAST KLINGON BATTLE CRUISER IN THE GALAXY HAS BEEN DESTROYED"
#4060  PRINT "THE FEDERATION HAS BEEN SAVED !!!"
#4070  PRINT
#4080  PRINT "YOUR EFFICIENCY RATING ="((K7/(T-T0))*1000)
#4090  T1=TIM(0)+TIM(1)*60
#4100  PRINT "YOUR ACTUAL TIME OF MISSION ="INT((((T1-T7)*.4)-T7)*100)" MINUTES"
#4110  GOTO 230
def end_game():
  print
  print "THE LAST KLINGON BATTLE CRUISER IN THE GALAXY HAS BEEN DESTROYED"
  print "THE FEDERATION HAS BEEN SAVED !!!"
  print
  print "YOUR EFFICIENCY RATING =" + ((K7/(T-T0))*1000)
  tm = datetime.datetime.now()
  T1 = tm.minute + 60*tm.hour
  print "YOUR ACTUAL TIME OF MISSION =" + int((((T1-T7)*.4) - T7) * 100) + " MINUTES"
  exit()

def galactic_record():
  print "COMPUTER RECORD OF GALAXY FOR QUADRANT %d,%d" % (Q1,Q2)
  #print "     1     2     3     4     5     6     7     8"
  print "     0     1     2     3     4     5     6     7"
  print "   ----- ----- ----- ----- ----- ----- ----- -----"
  for i in range(8):
    # might be nice to do blanks for ones that haven't been recorded yet
    # doesn't appear to be part of original program, though
    print "%d   %03d   %03d   %03d   %03d   %03d   %03d   %03d   %03d" % (i,Z[i,0],Z[i,1],Z[i,2],Z[i,3],Z[i,4],Z[i,5],Z[i,6],Z[i,7])
    print "   ----- ----- ----- ----- ----- ----- ----- -----"
  return

def status_report():
  print
  print "   STATUS REPORT"
  print
  print "NUMBER OF KLINGONS LEFT =" + str(K9)
  print "NUMBER OF STARDATES LEFT =" + str(T0+T9-T)
  print "NUMBER OF STARBASES LEFT =" + str(B9)
  damage_ctrl()

def calc_traj(C1,A,W1,X):
  X = X - A     # X is horizontal diff
  A = C1 - W1   # A is vertical diff
  if X == 0 and A == 0:
    return
  if X < 0:
    if A > 0:
      C1 = 3
      if abs(A) >= abs(X):
        print "DIRECTION = " + str(C1+(abs(X)/abs(A)))
      else:
        print "DIRECTION = " + str(C1+(((abs(X)-abs(A))+abs(X))/abs(X)))
    else:
      C1 = 5
      if abs(A) <= abs(X):
        print "DIRECTION = " + str(C1+(abs(A)/abs(X)))
      else:
        print "DIRECTION = " + str(C1+(((abs(A)-abs(X))+abs(A))/abs(A)))
  elif A < 0:
    C1 = 7
    if abs(A) >= abs(X):
      print "DIRECTION = " + str(C1+(abs(X)/abs(A)))
    else:
      print "DIRECTION = " + str(C1+(((abs(X)-abs(A))+abs(X))/abs(X)))
  elif X > 0:
    C1 = 1
    if abs(A) <= abs(X):
      print "DIRECTION = " + str(C1+(abs(A)/abs(X)))
    else:
      print "DIRECTION = " + str(C1+(((abs(A)-abs(X))+abs(A))/abs(A)))
  elif A == 0:
    C1 = 5
    if abs(A) <= abs(X):
      print "DIRECTION = " + str(C1+(abs(A)/abs(X)))
    else:
      print "DIRECTION = " + str(C1+(((abs(A)-abs(X))+abs(A))/abs(A)))
  else:
    C1 = 1
    if abs(A) <= abs(X):
      print "DIRECTION = " + str(C1+(abs(A)/abs(X)))
    else:
      print "DIRECTION = " + str(C1+(((abs(A)-abs(X))+abs(A))/abs(A)))
  print "DISTANCE = " + str(math.sqrt(X**2+A**2))

def torpedo_data():
  print
  H8 = 0
  for i in range(3):
    if K[i,2] > 0:
      C1 = S1
      A = S2
      W1 = K[i,0]
      X = K[i,1]
      calc_traj(C1,A,W1,X)
      if H8 is 1:
        return
  H8 = 0
  print "DO YOU WANT TO USE THE CALCULATOR"
  Astr = raw_input("")
  if Astr == "YES":
    print "YOU ARE AT QUADRANT ( %d,%d )  SECTOR ( %d,%d )" % (Q1,Q2,S1,S2)
    print "SHIP\'S & TARGET\'S COORINATES ARE"
    while True:
      try:
        C1 = int(raw_input("SHIP X"))
        A = int(raw_input("SHIP Y"))
        W1 = int(raw_input("TARGET X"))
        X = int(raw_input("TARGET Y"))
        break
      except:
        pass
    calc_traj(C1,A,W1,X)
    if H8 is 1:
      return
  else:
    return

# CALL ON LIBRARY COMPUTER
#4630  IF D[8] >= 0 THEN 4660
#4640  PRINT "COMPUTER DISABLED"
#4650  GOTO 1270
#4660  PRINT "COMPUTER ACTIVE AND AWAITING COMMAND";
#4670  INPUT A
#4680  GOTO A+1 OF 4740,4830,4880
#4690  PRINT "FUNCTIONS AVAILABLE FROM COMPUTER"
#4700  PRINT "   0 = CUMULATIVE GALACTIC RECORD"
#4710  PRINT "   1 = STATUS REPORT"
#4720  PRINT "   2 = PHOTON TORPEDO DATA"
#4730  GOTO 4660
#4740  PRINT  USING 4750;Q1,Q2
#4750  IMAGE  "COMPUTER RECORD OF GALAXY FOR QUADRANT ",D,",",D
#4760  PRINT  USING 5330
#4770  PRINT  USING 5360
#4780  FOR I=1 TO 8
#4790  PRINT  USING 5350;I,Z[I,1],Z[I,2],Z[I,3],Z[I,4],Z[I,5],Z[I,6],Z[I,7],Z[I,8]
#4800  PRINT  USING 5360
#4810  NEXT I
#4820  GOTO 1270
#4830  PRINT "\012   STATUS REPORT\012"
#4840  PRINT "NUMBER OF KLINGONS LEFT ="K9
#4850  PRINT "NUMBER OF STARDATES LEFT ="(T0+T9)-T
#4860  PRINT "NUMBER OF STARBASES LEFT ="B9
#4870  GOTO 3560
#4880  PRINT 
#4890  H8=0
#4900  FOR I=1 TO 3
#4910  IF K[I,3] <= 0 THEN 5260
#4920  C1=S1
#4930  A=S2
#4940  W1=K[I,1]
#4950  X=K[I,2]
#4960  GOTO 5010
#4970  PRINT  USING 4980;Q1,Q2,S1,S2
#4980  IMAGE  "YOU ARE AT QUADRANT ( ",D,",",D," )  SECTOR ( ",D,",",D," )"
#4990  PRINT "SHIP'S & TARGET'S COORDINATES ARE";
#5000  INPUT C1,A,W1,X
#5010  X=X-A
#5020  A=C1-W1
#5030  IF X<0 THEN 5130
#5040  IF A<0 THEN 5190
#5050  IF X>0 THEN 5070
#5060  IF A=0 THEN 5150
#5070  C1=1
#5080  IF ABS(A) <= ABS(X) THEN 5110
#5090  PRINT "DIRECTION ="C1+(((ABS(A)-ABS(X))+ABS(A))/ABS(A))
#5100  GOTO 5240
#5110  PRINT "DIRECTION ="C1+(ABS(A)/ABS(X))
#5120  GOTO 5240
#5130  IF A>0 THEN 5170
#5140  IF X=0 THEN 5190
#5150  C1=5
#5160  GOTO 5080
#5170  C1=3
#5180  GOTO 5200
#5190  C1=7
#5200  IF ABS(A) >= ABS(X) THEN 5230
#5210  PRINT "DIRECTION ="C1+(((ABS(X)-ABS(A))+ABS(X))/ABS(X))
#5220  GOTO 5240
#5230  PRINT "DIRECTION ="C1+(ABS(X)/ABS(A))
#5240  PRINT "DISTANCE ="(SQR(X^2+A^2))
#5250  IF H8=1 THEN 5320
#5260  NEXT I
#5270  H8=0
#5280  PRINT "DO YOU WANT TO USE THE CALCULATOR";
#5290  INPUT A$
#5300  IF A$="YES" THEN 4970
#5310  IF A$ <> "NO" THEN 5280
#5320  GOTO 1270
#5330  IMAGE  "     1     2     3     4     5     6     7     8"
#5340  IMAGE  "---------------------------------------------------"
#5350  IMAGE  D,8(3X,3D)
#5360  IMAGE  "   ----- ----- ----- ----- ----- ----- ----- -----"
def call_computer():
  if D[7] < 0:
    print "COMPUTER DISABLED"
    return
  computing = 1
  while computing:
    print "COMPUTER ACTIVE AND AWAITING COMMAND"
    com = raw_input("")
    if com is "0":
      galactic_record()
      computing = 0
    elif com is "1":
      status_report()
      computing = 0
    elif com is "2":
      torpedo_data()
      computing = 0
    else:
      print "FUNCTIONS AVAILABLE FROM COMPUTER"
      print "   0 = CUMULATIVE GALACTIC RECORD"
      print "   1 = STATUS REPORT"
      print "   2 = PHOTON TORPEDO DATA"
  return

#5370  IMAGE  " WARP ENGINES SHUTDOWN AT SECTOR ",D,",",D," DUE TO BAD NAVIGATION"

# random layout of elements in quadrant
#5380  R1=INT(RND(1)*8+1)
#5390  R2=INT(RND(1)*8+1)
#5400  A$="   "
#5410  Z1=R1
#5420  Z2=R2
#5430  GOSUB 5680
#5440  IF Z3=0 THEN 5380
#5450  RETURN
def sub1(Qstr,Rstr,Sstr):
  Z3 = 1
  while Z3 is 1:
    R1 = int(random.random()*8)
    R2 = int(random.random()*8)
    Astr = "   "
    Z1 = R1
    Z2 = R2
    Z3 = str_cmp_quadrant(Qstr,Rstr,Sstr,R1,R2,Astr)
  return (R1,R2)

#5460  FOR I=1 TO 11
#5470  PRINT
#5480  NEXT I
#5490  PRINT
#5500  RETURN
def blanks():
  for i in range(11):
    print
  print

#5510  REM ******  INSERTION IN STRING ARRAY FOR QUADRANT ******
#5520  S8=Z1*24+Z2*3-26
#5530  IF S8>72 THEN 5560
#5540  Q$[S8,S8+2]=A$
#5550  GOTO 5600
#5560  IF S8>144 THEN 5590
#5570  R$[S8-72,S8-70]=A$
#5580  GOTO 5600
#5590  S$[S8-144,S8-142]=A$
#5600  RETURN
def str_array_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr):
  S8 = Z1 * 24 + Z2 * 3
  if S8 < 72:
    Qstr = list(Qstr)
    Qstr[S8:S8+3] = Astr
    Qstr = "".join(Qstr)
  elif S8 < 144:
    Rstr = list(Rstr)
    Rstr[S8-72:S8-69] = Astr
    Rstr = "".join(Rstr)
  else:
    Sstr = list(Sstr)
    Sstr[S8-144:S8-141] = Astr
    Sstr = "".join(Sstr)
  return (Qstr, Rstr, Sstr)

#5610  REM ****  PRINTS DEVICE NAME FROM ARRAY *****
#5620  S8=R1*12-11
#5630  IF S8>72 THEN 5660
#5640  PRINT D$[S8,S8+11];
#5650  GOTO 5670
#5660  PRINT E$[S8-72,S8-61];
#5670  RETURN
def print_device_name(R1):
  S8 = R1 * 12 - 11
  if S8 > 72:
    print Estr[S8-73:S8-61],
  else:
    print Dstr[S8-1:S8+11],
    
#5680  REM *******  STRING COMPARISON IN QUADRANT ARRAY **********
#5683  Z1=INT(Z1+.5)
#5686  Z2=INT(Z2+.5)
#5690  S8=Z1*24+Z2*3-26
#5700  Z3=0
#5710  IF S8>72 THEN 5750
#5720  IF Q$[S8,S8+2] <> A$ THEN 5810
#5730  Z3=1
#5740  GOTO 5810
#5750  IF S8>144 THEN 5790
#5760  IF R$[S8-72,S8-70] <> A$ THEN 5810
#5770  Z3=1
#5780  GOTO 5810
#5790  IF S$[S8-144,S8-142] <> A$ THEN 5810
#5800  Z3=1
#5810  RETURN
# see if something is already in that spot
# based on Astr, Z1, Z2
# Z3 = 0 means match
def str_cmp_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr):
  #Z1 = int(Z1+.5)
  #Z2 = int(Z2+.5)
  S8 = Z1 * 24 + Z2 * 3
  Z3 = 1
  if S8 < 72:
    Qstr = list(Qstr)
    Qsub = "".join(Qstr[S8:S8+3])
    Qstr = "".join(Qstr)
    #print "\"" + binascii.hexlify(Qsub) + "\" " + "\"" + binascii.hexlify(Astr) + "\""
    if Qsub == Astr:
      Z3 = 0
      return Z3
    return Z3
  elif S8 < 144:
    Rstr = list(Rstr)
    Rsub = "".join(Rstr[S8-72:S8-69])
    Rstr = "".join(Rstr)
    #print "\"" + binascii.hexlify(Rsub) + "\" " + "\"" + binascii.hexlify(Astr) + "\""
    if Rsub == Astr:
      Z3 = 0
      return Z3
    return Z3
  else:
    Sstr = list(Sstr)
    Ssub = "".join(Sstr[S8-144:S8-141])
    Sstr = "".join(Sstr)
    #print "\"" + binascii.hexlify(Ssub) + "\" " + "\"" + binascii.hexlify(Astr) + "\""
    if Ssub == Astr:
      Z3 = 0
      return Z3
    return Z3
  return Z3

#5820  PRINT "     INSTRUCTIONS:"
#5830  PRINT "<*> = ENTERPRISE"
#5840  PRINT "+++ = KLINGON"
#5850  PRINT ">!< = STARBASE"
#5860  PRINT " *  = STAR"
#5870  PRINT "COMMAND 0 = WARP ENGINE CONTROL"
#5880  PRINT "  'COURSE' IS IN A CIRCULAR NUMERICAL          4  3  2"
#5890  PRINT "  VECTOR ARRANGEMENT AS SHOWN.                  \ ^ /"
#5900  PRINT "  INTERGER AND REAL VALUES MAY BE                \^/"
#5910  PRINT "  USED.  THEREFORE COURSE 1.5 IS              5 ----- 1"
#5920  PRINT "  HALF WAY BETWEEN 1 AND 2.                      /^\"
#5930  PRINT "                                                / ^ \"
#5940  PRINT "  A VECTOR OF 9 IS UNDEFINED, BUT              6  7  8"
#5950  PRINT "  VALUES MAY APPROACH 9."
#5960  PRINT "                                               COURSE"
#5970  PRINT "  ONE 'WARP FACTOR' IS THE SIZE OF"
#5980  PRINT "  ONE QUADRANT.  THEREFORE TO GET"
#5990  PRINT "  FROM QUADRANT 6,5 TO 5,5 YOU WOULD"
#6000  PRINT "  USE COURSE 3, WARP FACTOR 1"
#6010  PRINT "COMMAND 1 = SHORT RANGE SENSOR SCAN"
#6020  PRINT "  PRINTS THE QUADRANT YOU ARE CURRENTLY IN, INCLUDING"
#6030  PRINT "  STARS, KLINGONS, STARBASES, AND THE ENTERPRISE; ALONG"
#6040  PRINT "  WITH OTHER PERTINATE INFORMATION."
#6050  PRINT "COMMAND 2 = LONG RANGE SENSOR SCAN"
#6060  PRINT "  SHOWS CONDITIONS IN SPACE FOR ONE QUADRANT ON EACH SIDE"
#6070  PRINT "  OF THE ENTERPRISE IN THE MIDDLE OF THE SCAN.  THE SCAN"
#6080  PRINT "  IS CODED IN THE FORM XXX, WHERE THE UNITS DIGIT IS THE"
#6090  PRINT "  NUMBER OF STARS, THE TENS DIGIT IS THE NUMBER OF STAR-"
#6100  PRINT "  BASES, THE HUNDREDS DIGIT IS THE NUMBER OF KLINGONS."
#6110  PRINT "COMMAND 3 = PHASER CONTROL"
#6120  PRINT "  ALLOWS YOU TO DESTROY THE KLINGONS BY HITTING HIM WITH"
#6130  PRINT "  SUITABLY LARGE NUMBERS OF ENERGY UNITS TO DEPLETE HIS "
#6140  PRINT "  SHIELD POWER.  KEEP IN MIND THAT WHEN YOU SHOOT AT"
#6150  PRINT "  HIM, HE GONNA DO IT TO YOU TOO."
#6160  PRINT "COMMAND 4 = PHOTON TORPEDO CONTROL"
#6170  PRINT "  COURSE IS THE SAME AS USED IN WARP ENGINE CONTROL"
#6180  PRINT "  IF YOU HIT THE KLINGON, HE IS DESTROYED AND CANNOT FIRE"
#6190  PRINT "  BACK AT YOU.  IF YOU MISS, HE WILL SHOOT HIS PHASERS AT"
#6200  PRINT "  YOU."
#6210  PRINT "   NOTE: THE LIBRARY COMPUTER (COMMAND 7) HAS AN OPTION"
#6220  PRINT "   TO COMPUTE TORPEDO TRAJECTORY FOR YOU (OPTION 2)."
#6230  PRINT "COMMAND 5 = SHIELD CONTROL"
#6240  PRINT "  DEFINES NUMBER OF ENERGY UNITS TO BE ASSIGNED TO SHIELDS"
#6250  PRINT "  ENERGY IS TAKEN FROM TOTAL SHIP'S ENERGY."
#6260  PRINT "COMMAND 6 = DAMAGE CONTROL REPORT"
#6270  PRINT "  GIVES STATE OF REPAIRS OF ALL DEVICES.  A STATE OF REPAIR"
#6280  PRINT "  LESS THAN ZERO SHOWS THAT THAT DEVICE IS TEMPORARALY"
#6290  PRINT "  DAMAGED."
#6300  PRINT "COMMAND 7 = LIBRARY COMPUTER"
#6310  PRINT "  THE LIBRARY COMPUTER CONTAINS THREE OPTIONS:"
#6320  PRINT "    OPTION 0 = CUMULATIVE GALACTIC RECORD"
#6330  PRINT "     SHOWS COMPUTER MEMORY OF THE RESULTS OF ALL PREVIOUS"
#6340  PRINT "     LONG RANGE SENSOR SCANS"
#6350  PRINT "    OPTION 1 = STATUS REPORT"
#6360  PRINT "     SHOWS NUMBER OF KLINGONS, STARDATES AND STARBASES"
#6370  PRINT "     LEFT."
#6380  PRINT "    OPTION 2 = PHOTON TORPEDO DATA"
#6390  PRINT "     GIVES TRAJECTORY AND DISTANCE BETWEEN THE ENTERPRISE"
#6400  PRINT "     AND ALL KLINGONS IN YOUR QUADRANT"
#6410  RETURN
def print_instr():
  print "     INSTRUCTIONS:"
  print "<*> = ENTERPRISE"
  print "+++ = KLINGON"
  print ">!< = STARBASE"
  print " *  = STAR"
  print "COMMAND 0 = WARP ENGINE CONTROL"
  print "  'COURSE' IS IN A CIRCULAR NUMERICAL          4  3  2"
  print "  VECTOR ARRANGEMENT AS SHOWN.                  \ ^ /"
  print "  INTERGER AND REAL VALUES MAY BE                \^/"
  print "  USED.  THEREFORE COURSE 1.5 IS              5 ----- 1"
  print "  HALF WAY BETWEEN 1 AND 2.                      /^\ "
  print "                                                / ^ \ "
  print "  A VECTOR OF 9 IS UNDEFINED, BUT              6  7  8"
  print "  VALUES MAY APPROACH 9."
  print "                                               COURSE"
  print "  ONE 'WARP FACTOR' IS THE SIZE OF"
  print "  ONE QUADRANT.  THEREFORE TO GET"
  print "  FROM QUADRANT 6,5 TO 5,5 YOU WOULD"
  print "  USE COURSE 3, WARP FACTOR 1"
  print "COMMAND 1 = SHORT RANGE SENSOR SCAN"
  print  "  PRINTS THE QUADRANT YOU ARE CURRENTLY IN, INCLUDING"
  print "  STARS, KLINGONS, STARBASES, AND THE ENTERPRISE; ALONG"
  print "  WITH OTHER PERTINATE INFORMATION."
  print "COMMAND 2 = LONG RANGE SENSOR SCAN"
  print "  SHOWS CONDITIONS IN SPACE FOR ONE QUADRANT ON EACH SIDE"
  print "  OF THE ENTERPRISE IN THE MIDDLE OF THE SCAN.  THE SCAN"
  print "  IS CODED IN THE FORM XXX, WHERE THE UNITS DIGIT IS THE"
  print "  NUMBER OF STARS, THE TENS DIGIT IS THE NUMBER OF STAR-"
  print "  BASES, THE HUNDREDS DIGIT IS THE NUMBER OF KLINGONS."
  print "COMMAND 3 = PHASER CONTROL"
  print "  ALLOWS YOU TO DESTROY THE KLINGONS BY HITTING HIM WITH"
  print "  SUITABLY LARGE NUMBERS OF ENERGY UNITS TO DEPLETE HIS "
  print "  SHIELD POWER.  KEEP IN MIND THAT WHEN YOU SHOOT AT"
  print "  HIM, HE GONNA DO IT TO YOU TOO."
  print "COMMAND 4 = PHOTON TORPEDO CONTROL"
  print "  COURSE IS THE SAME AS USED IN WARP ENGINE CONTROL"
  print "  IF YOU HIT THE KLINGON, HE IS DESTROYED AND CANNOT FIRE"
  print "  BACK AT YOU.  IF YOU MISS, HE WILL SHOOT HIS PHASERS AT"
  print "  YOU."
  print "   NOTE: THE LIBRARY COMPUTER (COMMAND 7) HAS AN OPTION"
  print "   TO COMPUTE TORPEDO TRAJECTORY FOR YOU (OPTION 2)."
  print "COMMAND 5 = SHIELD CONTROL"
  print "  DEFINES NUMBER OF ENERGY UNITS TO BE ASSIGNED TO SHIELDS"
  print "  ENERGY IS TAKEN FROM TOTAL SHIP'S ENERGY."
  print "COMMAND 6 = DAMAGE CONTROL REPORT"
  print "  GIVES STATE OF REPAIRS OF ALL DEVICES.  A STATE OF REPAIR"
  print "  LESS THAN ZERO SHOWS THAT THAT DEVICE IS TEMPORARALY"
  print "  DAMAGED."
  print "COMMAND 7 = LIBRARY COMPUTER"
  print "  THE LIBRARY COMPUTER CONTAINS THREE OPTIONS:"
  print "    OPTION 0 = CUMULATIVE GALACTIC RECORD"
  print "     SHOWS COMPUTER MEMORY OF THE RESULTS OF ALL PREVIOUS"
  print "     LONG RANGE SENSOR SCANS"
  print "    OPTION 1 = STATUS REPORT"
  print "     SHOWS NUMBER OF KLINGONS, STARDATES AND STARBASES"
  print "     LEFT."
  print "    OPTION 2 = PHOTON TORPEDO DATA"
  print "     GIVES TRAJECTORY AND DISTANCE BETWEEN THE ENTERPRISE"
  print "     AND ALL KLINGONS IN YOUR QUADRANT"

# Main
#170  GOSUB 5460
blanks()

#180  PRINT "                          STAR TREK "
#190  PRINT "DO YOU WANT INSTRUCTIONS (THEY'RE LONG!)";
#200  INPUT A$
#210  IF A$ <> "YES" THEN 230
#220  GOSUB 5820
print "                          STAR TREK "
print "DO YOU WANT INSTRUCTIONS (THEY'RE LONG!)"
#instr = sys.stdin.readline()
instr = raw_input("")
if (instr == "YES"):
  print_instr()

#230  REM *****  PROGRAM STARTS HERE *****
#240  Z$="                                                                      "
#250  GOSUB 5460
Zstr = "                                                                       "
blanks()

#260  DIM G[8,8],C[9,2],K[3,3],N[3],Z[8,8]
#270  DIM C$[6],D$[72],E$[24],A$[3],Q$[72],R$[72],S$[48]
#280  DIM Z$[72]
#290  T0=T=INT(RND(1)*20+20)*100
#300  T9=30
#310  D0=0
#320  E0=E=3000
#330  P0=P=10
#340  S9=200
#350  S=H8=0
global G
G = numpy.empty((8,8), dtype=object)
# G is the number of klingons, bases, and stars per quadrant
C = numpy.empty((9,2), dtype=object)
K = numpy.empty((3,3), dtype=object)
N = numpy.empty((3), dtype=object)
Z = numpy.empty((8,8), dtype=object)
# Z is the sector layout of klingons, bases, and stars per quadrant
Cstr = "      "
Dstr = "                                                                       "
Estr = "                        "
Astr = "   "
Qstr = "                                                                        "
Rstr = "                                                                        "
Sstr = "                                                 "
Zstr = "                                                                        "
#T0 is initial time
#T is current time
T0 = T = int(((random.random()*20+20)*100))
#T0 = T = random.randint(2000,4000)
#T9 is num stardates left
T9 = 30
D0 = 0
#E is energy
global E
E0 = E = 3000
#P is photon torpedos
P0 = P = 10
S9 = 200
global S
S = H8 = 0

#360  DEF FND(D)=SQR((K[I,1]-S1)^2+(K[I,2]-S2)^2)
# quadratic distance between my sector and klingon in my sector
def fnd(i):
  
  return math.sqrt((K[i,0]-S1)**2+(K[i,1]-S2)**2)

#370  Q1=INT(RND(1)*8+1)
#380  Q2=INT(RND(1)*8+1)
#390  S1=INT(RND(1)*8+1)
#400  S2=INT(RND(1)*8+1)
#410  T7=TIM(0)+60*TIM(1)	# TIM(0) means current minute
#420  C[2,1]=C[3,1]=C[4,1]=C[4,2]=C[5,2]=C[6,2]=-1
#430  C[1,1]=C[3,2]=C[5,1]=C[7,2]=C[9,1]=0
#440  C[1,2]=C[2,2]=C[6,1]=C[7,1]=C[8,1]=C[8,2]=C[9,2]=1
#450  MAT D=ZER
#460  D$="WARP ENGINESS.R. SENSORSL.R. SENSORSPHASER CNTRL"
#470  D$[49]="PHOTON TUBESDAMAGE CNTRL"
#480  E$="SHIELD CNTRLCOMPUTER    "
#490  B9=K9=0
# Random initial starting position
global Q1
global Q2
global S1
global S2
Q1 = int((random.random()*8))
Q2 = int((random.random()*8))
S1 = int((random.random()*8))
S2 = int((random.random()*8))
# T7 tracks real-world duration
tm = datetime.datetime.now()
T7 = tm.minute + 60*tm.hour
C[1,0] = C[2,0] = C[3,0] = C[3,1] = C[4,1] = C[5,1] = -1
C[0,0] = C[2,1] = C[4,0] = C[6,1] = C[8,0] = 0
C[0,1] = C[1,1] = C[5,0] = C[6,0] = C[7,0] = C[7,1] = C[8,1] = 1
global D
D = numpy.zeros(8)
Dstr = "WARP ENGINESS.R. SENSORSL.R. SENSORSPHASER CNTRLPHOTON TUBESDAMAGE CNTRL"
Estr = "SHIELD CNTRLCOMPUTER    "
#K9 is cur num Klingons
#B9 is cur num bases
B9 = K9 = 0

#500  FOR I=1 TO 8
#510  FOR J=1 TO 8
#520  R1=RND(1)
#530  IF R1>.98 THEN 580
#540  IF R1>.95 THEN 610
#550  IF R1>.8 THEN 640
#560  K3=0
#570  GOTO 660
#580  K3=3
#590  K9=K9+3
#600  GOTO 660
#610  K3=2
#620  K9=K9+2
#630  GOTO 660
#640  K3=1
#650  K9=K9+1
#660  R1=RND(1)
#670  IF R1>.96 THEN 700
#680  B3=0
#690  GOTO 720
#700  B3=1
#710  B9=B9+1
#720  S3=INT(RND(1)*8+1)
#730  G[I,J]=K3*100+B3*10+S3
#740  Z[I,J]=0
#750  NEXT J
#760  NEXT I
#770  K7=K9
#775  IF B9 <= 0 OR K9 <= 0 THEN 490
good_game = 0
while not good_game:
  # for each quadrant
  for i in range(8):
    for j in range(8):
      # pick a number of klingons, weighted towards zero
      R1 = random.random()
      if R1 > .98:
        K3 = 3
        K9 = K9 + 3
      elif R1 > .95:
        K3 = 2
        K9 = K9 + 2
      elif R1 > .8:
        K3 = 1
        K9 = K9 + 1
      else:
        K3 = 0
      # pick a number of bases, weighted towards zero
      R1 = random.random()
      if R1 > .96:
        B3 = 1
        B9 = B9 + 1
      else:
        B3 = 0
      # pick a number of stars, weighted towards 8
      S3 = int(random.random()*8+1)
      # 534 means 5 klingons 3 bases 4 stars
      G[i,j] = K3*100+B3*10+S3
      Z[i,j] = 0
  # K7 is the number of klingons at the start of the game - used for stats at the end
  K7 = K9
  if (B9 > 0 and K9 > 0):
    good_game = 1

#780  PRINT "YOU MUST DESTROY"K9;" KLINGONS IN"T9;" STARDATES WITH"B9;" STARBASES"
#810  K3=B3=S3=0
# i think this should never happen
#820  IF Q1<1 OR Q1>8 OR Q2<1 OR Q2>8 THEN 920
#830  X=G[Q1,Q2]*.01
#840  K3=INT(X)
#850  B3=INT((X-K3)*10)
#860  S3=G[Q1,Q2]-INT(G[Q1,Q2]*.1)*10
#870  IF K3=0 THEN 910
#880  IF S>200 THEN 910
#890  PRINT "COMBAT AREA      CONDITION RED"
#900  PRINT "   SHIELDS DANGEROUSLY LOW"
#910  MAT K=ZER
print "YOU MUST DESTROY " + str(K9) + " KLINGONS IN " + str(T9) + " STARDATES WITH " + str(B9) + " STARBASES"

def draw_current_sector():
  global K3
  global B3
  global S3
  global Q1
  global Q2
  global S1
  global S2
  global G
  global S
  global K
  global Qstr
  global Rstr
  global Sstr
  K3 = B3 = S3 = 0
  # K3 B3 S3 are Klingons, Bases, and Stars in the current sector
  if Q1 >= 0 and Q1 <= 7 and Q2 >= 0 and Q2 <= 7:
    # grab 100s from G
    X = G[Q1,Q2]*.01
    K3 = int(X)
    B3 = int((X-K3)*10)
    S3 = G[Q1,Q2] - int(G[Q1,Q2]*.1)*10
    if K3 != 0:
      if S <= 200:
        print "COMBAT AREA      CONDITION RED"
        print "   SHIELDS DANGEROUSLY LOW"
    K = numpy.zeros((3,3))
  #920  FOR I=1 TO 3
  #930  K[I,3]=0
  #940  NEXT I
  for i in range(3):
    K[i,2] = 0
  # draw enterprise
  #950  Q$=Z$
  #960  R$=Z$
  #970  S$=Z$[1,48]
  #980  A$="<*>"
  #990  Z1=S1
  #1000  Z2=S2
  #1010  GOSUB 5510
  # blank out Q R S
  Qstr = Zstr
  Rstr = Zstr
  #Sstr = Zstr[0:47]
  Sstr = Zstr
  Astr = "<*>"
  Z1 = S1
  Z2 = S2
  Qstr, Rstr, Sstr = str_array_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
  # draw klingons per row of sectors
  #1020  FOR I=1 TO K3
  #1030  GOSUB 5380
  #1040  A$="+++"
  #1050  Z1=R1
  #1060  Z2=R2
  #1070  GOSUB 5510
  #1080  K[I,1]=R1
  #1090  K[I,2]=R2
  #1100  K[I,3]=S9
  #1110  NEXT I
  for i in range(K3):
    R1,R2 = sub1(Qstr,Rstr,Sstr)
    Astr = "+++"
    Z1 = R1
    Z2 = R2
    Qstr, Rstr, Sstr = str_array_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
    K[i,0] = R1
    K[i,1] = R2
    K[i,2] = S9
  # draw bases per row of sectors
  #1120  FOR I=1 TO B3
  #1130  GOSUB 5380
  #1140  A$=">!<"
  #1150  Z1=R1
  #1160  Z2=R2
  #1170  GOSUB 5510
  #1180  NEXT I
  for i in range(B3):
    R1,R2 = sub1(Qstr,Rstr,Sstr)
    Astr = ">!<"
    Z1 = R1
    Z2 = R2
    Qstr, Rstr, Sstr = str_array_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)
  # draw stars per row of sectors
  #1190  FOR I=1 TO S3
  #1200  GOSUB 5380
  #1210  A$=" * "
  #1220  Z1=R1
  #1230  Z2=R2
  #1240  GOSUB 5510
  #1250  NEXT I
  for i in range(S3):
    R1,R2 = sub1(Qstr,Rstr,Sstr)
    Astr = " * "
    Z1 = R1
    Z2 = R2
    Qstr, Rstr, Sstr = str_array_quadrant(Qstr,Rstr,Sstr,Z1,Z2,Astr)

draw_current_sector()

#1260  GOSUB 4120
#1270  PRINT "COMMAND:";
#1280  INPUT A
#1290  GOTO A+1 OF 1410,1260,2330,2530,2800,3460,3560,4630
#1300  PRINT
#1310  PRINT "   0 = SET COURSE"
#1320  PRINT "   1 = SHORT RANGE SENSOR SCAN"
#1330  PRINT "   2 = LONG RANGE SENSOR SCAN"
#1340  PRINT "   3 = FIRE PHASERS"
#1350  PRINT "   4 = FIRE PHOTON TORPEDOES"
#1360  PRINT "   5 = SHIELD CONTROL"
#1370  PRINT "   6 = DAMAGE CONTROL REPORT"
#1380  PRINT "   7 = CALL ON LIBRARY COMPUTER"
#1390  PRINT
#1400  GOTO 1270
game_over = False
srs_scan()
while not game_over:
  print "COMMAND:"
  com = raw_input("")
  if com is "0":
    set_course()
  elif com is "1":
    srs_scan()
  elif com is "2":
    lrs_scan()
  elif com is "3":
    fire_phasers()
  elif com is "4":
    fire_photons()
  elif com is "5":
    shield_ctrl()
  elif com is "6":
    damage_ctrl()
  elif com is "7":
    call_computer()
  elif com is "q":
    exit()
  print
  print "   0 = SET COURSE"
  print "   1 = SHORT RANGE SENSOR SCAN"
  print "   2 = LONG RANGE SENSOR SCAN"
  print "   3 = FIRE PHASERS"
  print "   4 = FIRE PHOTON TORPEDOES"
  print "   5 = SHIELD CONTROL"
  print "   6 = DAMAGE CONTROL REPORT"
  print "   7 = CALL ON LIBRARY COMPUTER"
  print

#6420  END
# comment out below
print "Program Ending"
